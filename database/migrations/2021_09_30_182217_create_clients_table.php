<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name')->default('');
            $table->string('street',100)->default('');
            $table->string('street_number',30)->default('');
            $table->string('city',100)->default('');
            $table->string('zip',20)->default('');
            $table->string('ico',20)->unique('ico');
            $table->string('dic',20)->default('');
            $table->string('icdph',20)->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
