<?php

namespace App\Models;

use App\Models\Nail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Wire extends Model
{
    use HasFactory;

    public function nails()
    {
        return $this->hasMany(Nail::class);
    }
}
