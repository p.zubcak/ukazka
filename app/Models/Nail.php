<?php

namespace App\Models;

use App\Models\Wire;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Nail extends Model
{
    use HasFactory;

    public function wire()
    {
        return $this->belongsTo(Wire::class);
    }

    public function changePriceCoefficient($priceCoefficient) {
        $this->price_coefficient = $priceCoefficient;
        $this->save();
    }

    public function changePricePerKgCoefficient($pricePerKgCoefficient) {
        $this->price_per_kg_coefficient = $pricePerKgCoefficient;
        $this->save();
    }

}
