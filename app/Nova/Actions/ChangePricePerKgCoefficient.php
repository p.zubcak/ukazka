<?php

namespace App\Nova\Actions;

use App\Models\Nail;
use Illuminate\Bus\Queueable;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangePricePerKgCoefficient extends Action
{
    use InteractsWithQueue, Queueable;

    public $name = "Zmena cenového koeficientu - Voľne sypané";

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model) {
            $model->changePricePerKgCoefficient($fields->price_per_kg_coefficient);
        }

        return  Action::message('Cenový koeficient bol úspešne uložený');
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Number::make('Cenový koeficient', 'price_per_kg_coefficient')->step(0.001)->min(0)->rules('required'),
        ];
    }
}
