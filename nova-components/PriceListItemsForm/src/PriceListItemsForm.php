<?php

namespace Archynet\PriceListItemsForm;

use App\Models\PriceList;
use Laravel\Nova\ResourceTool;
use Illuminate\Support\Facades\Config;

class PriceListItemsForm extends ResourceTool
{
    private $resource = null;

    public function __construct($resource)
    {
        $this->resource = $resource;

        parent::__construct();

        $this->withMeta([
            'nailWires' => $this->nailWires(),
            'nailFinishies' => $this->nailFinishies(),
            'isLast' => $this->resource->isLast(),
            'priceListData' => $this->resource,
            'constants' => Config::get('constants'),
        ]);
    }
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Price List Items Form';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'price-list-items-form';
    }

    public function nailFinishies()
    {
        return \App\Models\NailFinish::select(['id','name'])->get();
    }

    public function nailWires()
    {
        return \App\Models\Wire::select('id','average as name')->get();
    }

}
