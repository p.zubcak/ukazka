<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\Nail;
use App\Models\Client;
use App\Models\PriceList;
use Illuminate\Http\Request;
use App\Models\PriceListItem;
use Illuminate\Support\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PriceListController extends Controller
{
    /**
     * funkcia vrati zoznam vsetkych klincov v json formate zoradenych podla dlzky
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getNails(Request $request): JsonResponse
    {

        $nails = Nail::with('wire')
            ->orderBy('length', 'asc')
            ->get();

        return response()->json($nails);
    }

    /**
     * Funkcia vrati zoznam poloziek cennika v json formate. Id cennika pride cez post ako priceListId,
     * v poste je aj filter na droty filterSelectedWires ktory obsahuje idecka drotov,
     *  podla ktorych sa maju polozky cennika odfiltrovat.
     * Polozky su zoradene podla priemeru drotu, dlzky a povrchovej upravy
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getPriceListItems(Request $request): JsonResponse
    {

        $priceListId = $request->post('priceListId');
        $filterSelectedWires = $request->post('filterSelectedWires');

        $items = PriceListItem::with('Nail')
            ->where('price_list_id', $priceListId)
            ->whereIn('nail_id', Nail::select(['id'])->whereIn('wire_id', $filterSelectedWires) )
            ->orderBy('wire_average', 'asc')
            ->orderBy('nail_length', 'asc')
            ->orderBy('nail_finish_label', 'asc')
            ->get();

        return response()->json($items);
    }

    /**
     * Funkcia na pridanie klincov do cennika, v requeste dostane id cennika - priceListId,
     * zoznam id vybranych klincov - selectedNails a zoznam id vybranych povrchovych uprav - selectedFinishes.
     * Najde cennik podla id a zavola funkciu cennika addNails do ktorej posle idecka klincov a povrchovych uprav
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addNailsToPriceList(Request $request): JsonResponse
    {

        $priceListId = $request->post('priceListId');
        $selectedNails = $request->post('selectedNails');
        $selectedFinishes = $request->post('selectedFinishes');

        $priceList = PriceList::find($priceListId);
        $priceList->addNails($selectedNails, $selectedFinishes);

        return response()->json(['success' => true]);
    }

    /**
     * Funkcia na ulozenie poloziek cennika. V requeste pride:
     * id cennika - priceListId,
     * zobrazenie poctu klincov v kartone v pdf = pdfShowNumberPerCarton
     * zobrazenie poctu klincov na palete v pdf = pdfShowNumberPerPallet
     * poznamky k cenniku - notes
     * polozky cennika - priceListItems
     * Najde sa cennik ulozia sa don udaje,
     * nasledne sa prejdu vsetky polozky a updatenu sa doslymi udajmi
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function savePriceListItems(Request $request): JsonResponse
    {

        // ulozenie parametrov k cenniku
        $priceListId = $request->post('priceListId');

        $pdfShowNumberPerCarton = $request->post('pdfShowNumberPerCarton');
        $pdfShowNumberPerPallet = $request->post('pdfShowNumberPerPallet');
        $notes = $request->post('notes');

        // ulozenie udajov cennika
        $priceList = PriceList::find($priceListId);
        $priceList->pdf_show_number_per_carton = $pdfShowNumberPerCarton;
        $priceList->pdf_show_number_per_pallet = $pdfShowNumberPerPallet;
        $priceList->notes = $notes;
        $priceList->save();

        // ulozenie poloziek
        $itemsData = $request->post('priceListItems');

        foreach($itemsData as $item) {
            $priceListItem = PriceListItem::find($item['id']);
            unset($item['id']);
            unset($item['created_at']);
            unset($item['updated_at']);
            unset($item['selected']);
            unset($item['nail']);
            $priceListItem->fill($item)->save();
        }

        return response()->json(['success' => true]);
    }

    /**
     * Funkcia vygeneruje pdf podla zadefinovanej sablony a vrati toto vygenerovane pdf ako response
     *
     * @param Request $request
     * @return Response
     */
    public function downloadPDF(Request $request): Response
    {

      $priceListId = $request->get('priceListId');

      $data = PriceList::with('client')->find($priceListId);
      $data->priceListItems = PriceListItem::where('price_list_id', $data->id)
        ->orderBy('wire_average', 'asc')
        ->orderBy('nail_length', 'asc')
        ->orderBy('nail_finish_label', 'asc')->get();

      $filename = "cennik_" . $data->client->name . "_". date("d_m_Y", strtotime($data->created_at)).".pdf";

      $filename = mb_ereg_replace("([^\w\d\-_~,;\[\]\(\).])", '', $filename);

      $pdf = PDF::loadView('price_list.pdf', [ 'data' => $data] );

      return $pdf->download($filename);
    }

    /**
     * Funkcia na zmazanie poloziek z cennika,
     * v requeste pridu idecka poloziek ktore sa maju zmazat - priceListItemIds
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deletePriceListItems(Request $request): JsonResponse
    {

        $priceListItemIds = $request->get('priceListItemIds');

        if (is_array($priceListItemIds) && count($priceListItemIds)>0) {
            PriceListItem::destroy($priceListItemIds);
        }

        return response()->json(['success' => true]);
    }

    /**
     * funkcia na vykonanie hromadnej akcie nad polozkami cennika, v requeste pride
     * idecka poloziek cennika - priceListItemIds
     * typ akcie - actionType ('koeficient', 'prirazka')
     * nova hodnota - actionValue
     *
     * na zakade typu sa nastavi hodnota pre prislusny stlpec vsetkym polozkam ktorych id prislo v requeste
     *
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function actionPriceListItems(Request $request): JsonResponse
    {

        $priceListItemIds = $request->post('priceListItemIds');
        $actionType = $request->post('actionType');
        $actionValue = $request->post('actionValue');

        if (is_array($priceListItemIds) && count($priceListItemIds)>0) {

            foreach($priceListItemIds as $itemId) {

                $priceListItem = PriceListItem::find($itemId);

                switch ($actionType) {
                    case 'koeficient': {
                        if ($priceListItem->priceList->type == 'welded') {
                            $priceListItem->nail_price_coefficient = (float)$actionValue;
                        }
                        if ($priceListItem->priceList->type == 'loose') {
                            $priceListItem->nail_price_per_kg_coefficient = (float)$actionValue;
                        }
                    }break;
                    case 'prirazka': {
                        $priceListItem->surcharge = (float)$actionValue;
                    }break;
                    default: break;
                }

                $priceListItem->save();

            }

        }

        return response()->json(['success' => true]);
    }


    /**
     * Funkcia vrati zoznam poslednych 10 cennikov, zoradenych podla datumu vytvorenia
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getLastPriceLists(Request $request): JsonResponse
    {

        $priceLists = PriceList::with('client')->orderByDesc('created_at')->limit(10)->get();

        return response()->json($priceLists);

    }

    /**
     * Funckia skopiruje cennik ktore id pride v requeste,
     * takisto skopiruje aj vsetky polozky cennika,
     * po skopirovani vrati url na novy cennik
     *
     * @param Request $request
     * @return string
     */
    public function copyPriceList(Request $request): string
    {

        // ulozenie parametrov k cenniku
        $priceListId = $request->get('priceListId');

        $priceList = PriceList::find($priceListId);

        $newPriceList = $priceList->replicate();
        $newPriceList->created_at = Carbon::now();
        $newPriceList->push();

        foreach ($priceList->priceListItems()->get() as $priceListItem) {
            $newPriceListItem = $priceListItem->replicate();
            $newPriceListItem->created_at = Carbon::now();
            $newPriceListItem->price_list_id = $newPriceList->id;
            // TODO: treba nastvait aktualnu cenu drotu a koeficienty
            $newPriceListItem->push();
        }

        return url("/nova/resources/price-lists/{$newPriceList->id}");

    }

    /* pouzite na jednorazovy import klientov
    public function import(Request $request)
    {
        return "aaa";
        //$file = public_path('clients.csv');
        $file = storage_path('app/public')."/clients.csv";

        $clientsCSV = $this->csvToArray($file);

        foreach($clientsCSV as $client) {
            $clientDB = Client::where('ico',$client['ico'])->first();
            if($clientDB===null ) {
                //neexistuje naimportujem
                unset($client['id']);
                unset($client['created_at']);
                unset($client['updated_at']);
                $client['surcharge'] = 0;
                $newClient = new Client();
                $newClient->fill($client);
                $newClient->save();
            }

        }

        return "aaa";
    }

    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header){
                    $header = $row;
                } else {
                    $row = array_map(function($item){ return trim($item);},$row);
                    $data[] = array_combine($header, $row);
                }

            }
            fclose($handle);
        }

        return $data;
    }
    */

}
