<?php

namespace App\Models;

use App\Models\Nail;
use App\Models\Client;
use App\Models\PriceListItem;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PriceList extends Model
{
    use HasFactory;

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function priceListItems()
    {
        return $this->hasMany(PriceListItem::class)
            ->orderBy('wire_average', 'asc')
            ->orderBy('nail_length', 'asc');
    }

    /**
     * Funkcia prida klince do cennika, to znamena ze vytvori polozky cennika z vybranych klincov
     *
     * @param array $selectedNails
     * @param array $selectedFinishes
     * @return void
     */
    public function addNails(array $selectedNails, array $selectedFinishes): void
    {

        /* vytiahnem si z db vybrane klince */
        $nails = Nail::whereIn('id', $selectedNails)->with('wire')->get();
        $defaultSurcharge = 0;
        if ($this->client->surcharge>0) {
            $defaultSurcharge = $this->client->surcharge;
        }

        /* vytiahnem si z cennika uz pridane klince */
        $existItems = PriceListItem::select(['id','nail_id','nail_finish_id'])->where('price_list_id', $this->id)->get()->toArray();
        $existItemsById = [];
        if (is_array($existItems)) {
            foreach($existItems as $ei) {
                $existItemsById[] = $ei['nail_id']."-".($ei['nail_finish_id']?$ei['nail_finish_id']:"0");
            }
        }

        if (is_array($selectedFinishes)) {
            foreach ($selectedFinishes as $selectedFinish) {
                $selectedFinish = (int)$selectedFinish;
                $finishCoefficient = Config::get('constants.nailNoFinishCoefficient');;
                $finishLabel = '';
                $finishId = null;
                if ($selectedFinish>0) {
                    $finish = NailFinish::find($selectedFinish);
                    $finishCoefficient = 0;
                    $finishLabel = $finish->label;
                    $finishId = $finish->id;
                }

                foreach ($nails as $nail) {

                    if (in_array($nail->id."-".$selectedFinish, $existItemsById)) continue;

                    $price1000 = 0;
                    $pricePerKg = 0;

                    $price1000 = (($nail->weight_1000/1000)*$nail->wire->price) + $nail->price_coefficient + $finishCoefficient;

                    $pricePerKg = ($nail->wire->price/1000) + $nail->price_per_kg_coefficient;

                    if ($defaultSurcharge>0) {
                        $price1000 = round($price1000*(1+($defaultSurcharge/100)), 2);
                        $pricePerKg = round($pricePerKg*(1+($defaultSurcharge/100)), 2);
                    }

                    $data = [
                        'price_list_id' => $this->id,
                        'nail_id' => $nail->id,
                        'nail_finish_id' => $finishId,
                        'wire_average' => $nail->wire->average,
                        'wire_price' => $nail->wire->price,
                        'nail_length' => $nail->length,
                        'nail_weight_1000' => $nail->weight_1000,
                        'nail_price_coefficient' => $nail->price_coefficient,
                        'nail_price_per_kg_coefficient' => $nail->price_per_kg_coefficient,
                        'nail_number_per_carton' => $nail->number_per_carton,
                        'nail_number_per_pallet' => $nail->number_per_pallet,
                        'nail_finish_label' => $finishLabel,
                        'surcharge' => $defaultSurcharge,
                        'price_1000' => $price1000,
                        'price_per_kg' => $pricePerKg,
                    ];

                    PriceListItem::create($data);

                    $existItemsById[] = $nail->id."-".$selectedFinish;


                }
            }
        }
    }

    /**
     * Funkcia vrati boolean hodnotu true v pripade ak je cennik poslednym pre daneho klienta z daneho typu,
     *  v opacnom pripade vrati false
     *
     * @return boolean
     */
    public function isLast(): bool
    {
        $last = PriceList::select(['id'])->where('client_id', $this->client_id)->where('type', $this->type)->orderByDesc('created_at')->first();

        return $this->id == $last->id;
    }


}
