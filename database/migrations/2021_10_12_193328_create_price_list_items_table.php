<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceListItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_list_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('price_list_id');
            $table->foreignId('nail_id');
            $table->foreignId('nail_finish_id')->nullable();
            $table->double('wire_average',8,2)->default(0);
            $table->double('wire_price')->default(0);
            $table->double('nail_length')->default(0);
            $table->double('nail_weight_1000')->default(0)->nullable();
            $table->double('nail_price_coefficient')->default(0)->nullable();
            $table->char('nail_category_name',255)->default('')->nullable();
            $table->char('nail_finish_label', 50)->default('')->nullable();
            $table->double('nail_finish_price_coefficient')->default(0);
            $table->double('surcharge')->default(0)->nullable();
            $table->double('price_1000')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_list_items');
    }
}
