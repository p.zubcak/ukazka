<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use App\Nova\Filters\Clients;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\BelongsTo;
use App\Nova\Filters\PriceListType;
use Archynet\LastPriceLists\LastPriceLists;
use Laravel\Nova\Http\Requests\NovaRequest;
use Archynet\PriceListItemsForm\PriceListItemsForm;

class PriceList extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\PriceList::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'client.name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'created_at', 'type'
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'client' => ['name', 'email'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $fields = [
            ID::make(__('ID'), 'id')->sortable()->hideFromDetail(),
            Select::make('Typ', 'type')
                ->required()
                ->options(['welded' => 'Zvárané', 'loose' => 'Voľne sypané'])
                ->displayUsingLabels()
                ->resolveUsing(function () {
                    return $this->type ?? 'welded';
                })
                ->sortable()
                ->hideFromDetail(),
            BelongsTo::make('Klient', 'client', 'App\Nova\Client')
                ->viewable(false)
                ->hideFromDetail()
                ->sortable()
                ->searchable(),
            DateTime::make('Vytvorené', 'created_at')->readonly()->hideFromDetail()->sortable(),
        ];

        if ($this->resource->exists) {
           $fields[] = PriceListItemsForm::make($this->resource);
        }

        return $fields;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Cenníky';
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Cenník';
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [

        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Clients,
            new PriceListType,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

}
