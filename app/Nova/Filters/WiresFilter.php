<?php

namespace App\Nova\Filters;

use App\Models\Wire;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class WiresFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    public $name = 'Drôty';
    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('wire_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $options = [];

        $wires = Wire::all(['average','id']);

        foreach($wires as $wire) {
            $options["{$wire['average']}"] = $wire['id'];
        }

        return $options;
    }
}
