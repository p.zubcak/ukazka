<?php

namespace App\Models;

use App\Models\Nail;
use App\Models\PriceList;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PriceListItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function priceList()
    {
        return $this->belongsTo(PriceList::class);
    }

    public function nail()
    {
        return $this->belongsTo(Nail::class);
    }

}
