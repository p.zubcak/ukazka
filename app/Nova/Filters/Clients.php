<?php

namespace App\Nova\Filters;

use App\Models\Client;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class Clients extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    public $name = 'Klienti';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('client_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $options = [];

        $clients = Client::all(['name','id']);

        foreach($clients as $client) {
            $options[$client['name']] = $client['id'];
        }

        return $options;
    }
}
