<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Number;
use App\Nova\Filters\WiresFilter;
use Laravel\Nova\Fields\BelongsTo;
use App\Nova\Filters\NailCategories;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Nova\Actions\ChangePriceCoefficient;
use App\Nova\Actions\ChangePricePerKgCoefficient;

class Nail extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Nail::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'wire_id', 'length', 'weight_1000', 'price_coefficient', 'price_per_kg_coefficient'
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'wire' => ['average'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            BelongsTo::make('Drôt', 'wire', 'App\Nova\Wire')->viewable(false)->sortable(),
            Number::make('Dĺžka', 'length')->sortable()->step(0.1),
            Number::make('Váha 1000ks', 'weight_1000')->sortable()->step(0.001),
            Number::make('Cenový koeficient - Zvárané', 'price_coefficient')->sortable()->step(0.001),
            Number::make('Cenový koeficient - Voľne sypané', 'price_per_kg_coefficient')->sortable()->step(0.001),
            Number::make('Počet v kartóne', 'number_per_carton')->sortable()->step(1),
            Number::make('Počet na palete', 'number_per_pallet')->sortable()->step(1),
        ];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Klince';
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Klinec';
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new WiresFilter,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            ChangePriceCoefficient::make()->onlyOnIndex(true),
            ChangePricePerKgCoefficient::make()->onlyOnIndex(true),
        ];
    }
}
