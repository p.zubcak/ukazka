<?php

use App\Http\Controllers\PriceListController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
*/

 Route::get('/get-nails', [PriceListController::class, 'getNails']);
 Route::post('/get-price-list-items', [PriceListController::class, 'getPriceListItems']);
 Route::post('/add-nails-to-price-list', [PriceListController::class, 'addNailsToPriceList']);
 Route::post('/save-price-list-items', [PriceListController::class, 'savePriceListItems']);
 Route::post('/get-price-list-pdf', [PriceListController::class, 'downloadPDF']);
 Route::delete('/delete-price-list-item', [PriceListController::class, 'deletePriceListItems']);
 Route::get('/copy-price-list', [PriceListController::class, 'copyPriceList']);
 Route::post('/action-price-list-items', [PriceListController::class, 'actionPriceListItems']);