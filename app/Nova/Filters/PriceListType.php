<?php

namespace App\Nova\Filters;

use App\Models\Client;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class PriceListType extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    public $name = 'Typ cenníka';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('type', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $options = [
            'Zvárané' => 'welded',
            'Voľne sypané' => 'loose',
        ];

        return $options;
    }
}
