/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/AddNailsWindow.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/AddNailsWindow.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: {
    nailWires: {
      type: Array,
      "default": function _default() {
        return [];
      }
    },
    nailFinishies: {
      type: Array,
      "default": function _default() {
        return [];
      }
    },
    priceListId: {}
  },
  data: function data() {
    return {
      selectedNails: [],
      selectedWires: [],
      selectedFinishes: [],
      nails: []
    };
  },
  computed: {
    nailsFiltered: function nailsFiltered() {
      var _this = this;

      var tmpNails = [];
      this.nails.forEach(function (nail) {
        if (_this.selectedWires.includes(nail.wire_id)) {
          tmpNails.push(nail);
        }
      });
      return tmpNails;
    }
  },
  watch: {
    selectedWires: function selectedWires() {
      this.deselectNailsInNonActiveWires();
    }
  },
  methods: {
    close: function close(refresh) {
      this.$emit('close', refresh);
    },
    addNailsToPriceList: function addNailsToPriceList() {
      var _this2 = this;

      var data = {};
      data.priceListId = this.priceListId;
      data.selectedNails = this.selectedNails;
      data.selectedFinishes = this.selectedFinishes;
      Nova.request().post('/nova-vendor/price-list-items-form/add-nails-to-price-list', data).then(function (response) {
        console.log(response);

        _this2.close(true);
      });
    },
    loadNails: function loadNails() {
      var _this3 = this;

      Nova.request().get('/nova-vendor/price-list-items-form/get-nails').then(function (response) {
        _this3.nails = response.data;

        _this3.nails.map(function (item) {
          item.checked = false;
          return item;
        });
      });
    },
    checkNail: function checkNail(id) {
      var tmp = this.selectedNails;

      if (tmp.includes(id)) {
        _.pull(tmp, id);
      } else {
        tmp.push(id);
      }

      this.selectedNails = tmp;
      this.$forceUpdate();
    },
    selectAllNails: function selectAllNails() {
      var _this4 = this;

      this.selectedNails = [];
      this.nails.forEach(function (nail) {
        if (_this4.selectedWires.includes(nail.wire_id)) {
          _this4.selectedNails.push(nail.id);
        }
      });
    },
    deselectAllNails: function deselectAllNails() {
      this.selectedNails = [];
    },
    deselectNailsInNonActiveWires: function deselectNailsInNonActiveWires() {
      var _this5 = this;

      this.nails.forEach(function (nail) {
        if (_this5.selectedNails.includes(nail.id) && !_this5.selectedWires.includes(nail.wire_id)) {
          _.pull(_this5.selectedNails, nail.id);
        }
      });
    },
    selectAllWires: function selectAllWires() {
      var _this6 = this;

      this.selectedWires = [];
      this.nailWires.forEach(function (cat) {
        _this6.selectedWires.push(cat.id);
      });
    },
    deselectAllWires: function deselectAllWires() {
      this.selectedWires = [];
      this.selectedNails = [];
    }
  },
  mounted: function mounted() {
    this.loadNails(); //this.selectAllWires();
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsForm.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsForm.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _AddNailsWindow_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddNailsWindow.vue */ "./resources/js/components/AddNailsWindow.vue");
/* harmony import */ var _PriceListItemsFormRow_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PriceListItemsFormRow.vue */ "./resources/js/components/PriceListItemsFormRow.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    AddNailsWindow: _AddNailsWindow_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    PriceListItemsFormRow: _PriceListItemsFormRow_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  props: ['resourceName', 'resourceId', 'panel'],
  data: function data() {
    return {
      showAddNailWindow: false,
      priceListData: {
        client: {}
      },
      priceListItems: [],
      pdfShowNumberPerCarton: false,
      pdfShowNumberPerPallet: false,
      notes: '',
      selectAllRows: false,
      multiDeleteDialogShow: false,
      filterSelectedWires: [],
      actionDialogShow: false,
      actionType: '',
      actionValue: ''
    };
  },
  watch: {
    selectAllRows: function selectAllRows(newVal, oldVal) {
      if (newVal != oldVal) {
        this.priceListItems.forEach(function (item) {
          item.selected = newVal;
        });
      }
    },
    filterSelectedWires: _.debounce(function () {
      var _this = this;

      this.savePriceListItems(function () {
        _this.loadPriceListItems();
      }, true);
    }, 1000)
  },
  computed: {
    priceListItemsHeader: function priceListItemsHeader() {
      var priceTitle = "";

      if (this.priceListData.type == 'welded') {
        priceTitle = 'Cena za 1000 ks [eur]';
      }

      if (this.priceListData.type == 'loose') {
        priceTitle = 'Cena za kg [eur]';
      }

      return [{
        title: 'Klinec'
      }, {
        title: 'Váha 1000 ks [kg]'
      }, {
        title: 'Cena drôtu [eur/t]'
      }, {
        title: 'Cenový koeficient'
      }, {
        title: 'Prirážka [%]'
      }, {
        title: priceTitle
      }];
    },
    priceListType: function priceListType() {
      var type = '-';

      if (this.priceListData.type == 'welded') {
        type = 'Zvárané';
      }

      if (this.priceListData.type == 'loose') {
        type = 'Voľne sypané';
      }

      return type;
    },
    readonly: function readonly() {
      return !this.panel.fields[0].isLast;
    },
    createdAt: function createdAt() {
      var cdate = new Date(this.priceListData.created_at);
      return cdate.getDate().toString().padStart(2, '0') + '.' + (cdate.getMonth() + 1).toString().padStart(2, '0') + '.' + cdate.getFullYear() + ' ' + cdate.getHours().toString().padStart(2, '0') + ':' + cdate.getMinutes().toString().padStart(2, '0');
    }
  },
  methods: {
    openActionDialog: function openActionDialog() {
      var selectedRows = [];
      this.priceListItems.forEach(function (item) {
        if (item.selected) {
          selectedRows.push(item.id);
        }
      });

      if (selectedRows.length > 0) {
        this.actionDialogShow = true;
      } else {
        this.$toasted.error('Neboli vybrané žiadne záznamy.');
      }
    },
    closeActionDialog: function closeActionDialog() {
      this.actionDialogShow = false;
    },
    doAction: function doAction() {
      var _this2 = this;

      var selectedRows = [];
      this.priceListItems.forEach(function (item) {
        if (item.selected) {
          selectedRows.push(item.id);
        }
      });
      var data = {
        priceListItemIds: selectedRows,
        actionType: this.actionType,
        actionValue: this.actionValue
      };
      Nova.request().post('/nova-vendor/price-list-items-form/action-price-list-items', data).then(function (response) {
        _this2.selectAllRows = false;
        _this2.actionType = '';
        _this2.actionValue = '';

        _this2.loadPriceListItems();

        _this2.closeActionDialog();
      });
    },
    openMultiDeleteDialog: function openMultiDeleteDialog() {
      var selectedRows = [];
      this.priceListItems.forEach(function (item) {
        if (item.selected) {
          selectedRows.push(item.id);
        }
      });

      if (selectedRows.length > 0) {
        this.multiDeleteDialogShow = true;
      } else {
        this.$toasted.error('Neboli vybrané žiadne záznamy.');
      }
    },
    closeMultiDeleteDialog: function closeMultiDeleteDialog() {
      this.multiDeleteDialogShow = false;
    },
    deleteSelectedItems: function deleteSelectedItems() {
      var _this3 = this;

      var selectedRows = [];
      this.priceListItems.forEach(function (item) {
        if (item.selected) {
          selectedRows.push(item.id);
        }
      });
      var config = {
        data: {
          priceListItemIds: selectedRows
        }
      };
      Nova.request()["delete"]('/nova-vendor/price-list-items-form/delete-price-list-item', config).then(function (response) {
        _this3.selectAllRows = false;

        _this3.loadPriceListItems();

        _this3.closeMultiDeleteDialog();
      });
    },
    openAddNailWindow: function openAddNailWindow() {
      this.showAddNailWindow = true;
    },
    closeAddNailWindow: function closeAddNailWindow(refresh) {
      if (refresh === true) {
        this.loadPriceListItems();
      }

      this.showAddNailWindow = false;
    },
    copyPriceList: function copyPriceList() {
      Nova.request().get('/nova-vendor/price-list-items-form/copy-price-list?priceListId=' + this.resourceId).then(function (response) {
        window.location.href = response.data;
      });
    },
    loadPriceListItems: function loadPriceListItems() {
      var _this4 = this;

      this.priceListItems = [];
      Nova.request().post('/nova-vendor/price-list-items-form/get-price-list-items', {
        priceListId: this.resourceId,
        filterSelectedWires: this.filterSelectedWires
      }).then(function (response) {
        _this4.priceListItems = response.data.map(function (obj) {
          var rObj = obj;
          rObj.selected = false;
          return rObj;
        });
      });
    },
    savePriceListItems: function savePriceListItems(onSuccess, silent) {
      var _this5 = this;

      var data = {};
      data.priceListId = this.resourceId;
      data.priceListItems = this.priceListItems;
      data.notes = this.notes;
      data.pdfShowNumberPerCarton = this.pdfShowNumberPerCarton;
      data.pdfShowNumberPerPallet = this.pdfShowNumberPerPallet;
      Nova.request().post('/nova-vendor/price-list-items-form/save-price-list-items', data).then(function (response) {
        if (!silent) {
          _this5.$toasted.success('Parametre boli úspešne uložené.');
        }

        if (typeof onSuccess == "function") {
          onSuccess(response);
        }
      });
    },
    savePriceListAndExport: function savePriceListAndExport() {
      var _this6 = this;

      this.savePriceListItems(function (response) {
        _this6.$toasted.success('Parametre boli úspešne uložené, prebieha export do PDF. Čakajte prosím.');

        _this6.exportPDF();
      }, true);
    },
    exportPDF: function exportPDF() {
      var _this7 = this;

      var data = {};
      data.priceListId = this.resourceId;
      Nova.request().post('/nova-vendor/price-list-items-form/get-price-list-pdf', data, {
        responseType: 'arraybuffer'
      }).then(function (response) {
        _this7.downloadFile(response);
      });
    },
    downloadFile: function downloadFile(response) {
      // It is necessary to create a new blob object with mime-type explicitly set
      // otherwise only Chrome works like it should
      var filename = response.headers['content-disposition'].split('=')[1].replace(/^\"+|\"+$/g, '');
      var newBlob = new Blob([response.data], {
        type: 'application/pdf'
      }); // IE doesn't allow using a blob object directly as link href
      // instead it is necessary to use msSaveOrOpenBlob

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      } // For other browsers:
      // Create a link pointing to the ObjectURL containing the blob.


      var data = window.URL.createObjectURL(newBlob);
      var link = document.createElement('a');
      link.href = data;
      link.download = filename;
      link.click();
      setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
      }, 100);
    }
  },
  mounted: function mounted() {
    var _this8 = this;

    this.panel.fields[0].nailWires.forEach(function (wireItem) {
      _this8.filterSelectedWires.push(wireItem.id);
    }); // nastavenie filtra sposobi spustenie udalosti loadpricelistitems
    //this.loadPriceListItems()

    this.priceListData = this.panel.fields[0].priceListData;
    this.pdfShowNumberPerCarton = this.panel.fields[0].priceListData.pdf_show_number_per_carton;
    this.pdfShowNumberPerPallet = this.panel.fields[0].priceListData.pdf_show_number_per_pallet;
    this.notes = this.panel.fields[0].priceListData.notes;
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsFormRow.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsFormRow.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: {
    value: {},
    readonly: {},
    constants: {},
    priceListType: ''
  },
  data: function data() {
    return {
      priceListItem: {},
      deleteDialogShow: false
    };
  },
  computed: {
    priceListItemName: function priceListItemName() {
      return this.priceListItem.wire_average + 'x' + this.priceListItem.nail_length + (this.priceListItem.nail_finish_label ? ' ' + this.priceListItem.nail_finish_label : '');
    },
    price_per_kg: function price_per_kg() {
      var price = 0;
      price = parseFloat(this.priceListItem.wire_price) / 1000 + parseFloat(this.priceListItem.nail_price_per_kg_coefficient);
      var surcharge = parseFloat(this.priceListItem.surcharge);

      if (surcharge > 0) {
        price = price * (1 + surcharge / 100);
      }

      price = price.toFixed(2);
      this.priceListItem.price_per_kg = price;
      return price;
    },
    price_1000: function price_1000() {
      var price = 0;
      price = parseFloat(this.priceListItem.nail_weight_1000) / 1000 * parseFloat(this.priceListItem.wire_price) + parseFloat(this.priceListItem.nail_price_coefficient) + parseFloat(this.constants.nailNoFinishCoefficient);
      var surcharge = parseFloat(this.priceListItem.surcharge);

      if (surcharge > 0) {
        price = price * (1 + surcharge / 100);
      }

      price = price.toFixed(2);
      this.priceListItem.price_1000 = price;
      return price;
    }
  },
  methods: {
    openDeleteDialog: function openDeleteDialog() {
      this.deleteDialogShow = true;
    },
    closeDeleteDialog: function closeDeleteDialog() {
      this.deleteDialogShow = false;
    },
    deleteItem: function deleteItem() {
      var _this = this;

      var config = {
        data: {
          priceListItemIds: [this.priceListItem.id]
        }
      };
      Nova.request()["delete"]('/nova-vendor/price-list-items-form/delete-price-list-item', config).then(function (response) {
        _this.closeDeleteDialog();

        _this.$emit('delete-row');

        console.log(response);
      });
    }
  },
  mounted: function mounted() {
    this.priceListItem = this.value;
  }
});

/***/ }),

/***/ "./resources/js/tool.js":
/*!******************************!*\
  !*** ./resources/js/tool.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_PriceListItemsForm_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/PriceListItemsForm.vue */ "./resources/js/components/PriceListItemsForm.vue");

Nova.booting(function (Vue, router, store) {
  Vue.component('price-list-items-form', _components_PriceListItemsForm_vue__WEBPACK_IMPORTED_MODULE_0__["default"]);
});

/***/ }),

/***/ "./resources/sass/tool.scss":
/*!**********************************!*\
  !*** ./resources/sass/tool.scss ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./resources/js/components/AddNailsWindow.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/AddNailsWindow.vue ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _AddNailsWindow_vue_vue_type_template_id_12623d37_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddNailsWindow.vue?vue&type=template&id=12623d37&scoped=true& */ "./resources/js/components/AddNailsWindow.vue?vue&type=template&id=12623d37&scoped=true&");
/* harmony import */ var _AddNailsWindow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddNailsWindow.vue?vue&type=script&lang=js& */ "./resources/js/components/AddNailsWindow.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AddNailsWindow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AddNailsWindow_vue_vue_type_template_id_12623d37_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _AddNailsWindow_vue_vue_type_template_id_12623d37_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "12623d37",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/AddNailsWindow.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/PriceListItemsForm.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/PriceListItemsForm.vue ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _PriceListItemsForm_vue_vue_type_template_id_470f9a08___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PriceListItemsForm.vue?vue&type=template&id=470f9a08& */ "./resources/js/components/PriceListItemsForm.vue?vue&type=template&id=470f9a08&");
/* harmony import */ var _PriceListItemsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PriceListItemsForm.vue?vue&type=script&lang=js& */ "./resources/js/components/PriceListItemsForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PriceListItemsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PriceListItemsForm_vue_vue_type_template_id_470f9a08___WEBPACK_IMPORTED_MODULE_0__.render,
  _PriceListItemsForm_vue_vue_type_template_id_470f9a08___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/PriceListItemsForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/PriceListItemsFormRow.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/PriceListItemsFormRow.vue ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _PriceListItemsFormRow_vue_vue_type_template_id_55edac3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PriceListItemsFormRow.vue?vue&type=template&id=55edac3c&scoped=true& */ "./resources/js/components/PriceListItemsFormRow.vue?vue&type=template&id=55edac3c&scoped=true&");
/* harmony import */ var _PriceListItemsFormRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PriceListItemsFormRow.vue?vue&type=script&lang=js& */ "./resources/js/components/PriceListItemsFormRow.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PriceListItemsFormRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PriceListItemsFormRow_vue_vue_type_template_id_55edac3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _PriceListItemsFormRow_vue_vue_type_template_id_55edac3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "55edac3c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/PriceListItemsFormRow.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/AddNailsWindow.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/AddNailsWindow.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNailsWindow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AddNailsWindow.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/AddNailsWindow.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNailsWindow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/PriceListItemsForm.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/PriceListItemsForm.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PriceListItemsForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/PriceListItemsFormRow.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/PriceListItemsFormRow.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsFormRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PriceListItemsFormRow.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsFormRow.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsFormRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/AddNailsWindow.vue?vue&type=template&id=12623d37&scoped=true&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/AddNailsWindow.vue?vue&type=template&id=12623d37&scoped=true& ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNailsWindow_vue_vue_type_template_id_12623d37_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNailsWindow_vue_vue_type_template_id_12623d37_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddNailsWindow_vue_vue_type_template_id_12623d37_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AddNailsWindow.vue?vue&type=template&id=12623d37&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/AddNailsWindow.vue?vue&type=template&id=12623d37&scoped=true&");


/***/ }),

/***/ "./resources/js/components/PriceListItemsForm.vue?vue&type=template&id=470f9a08&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/PriceListItemsForm.vue?vue&type=template&id=470f9a08& ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsForm_vue_vue_type_template_id_470f9a08___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsForm_vue_vue_type_template_id_470f9a08___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsForm_vue_vue_type_template_id_470f9a08___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PriceListItemsForm.vue?vue&type=template&id=470f9a08& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsForm.vue?vue&type=template&id=470f9a08&");


/***/ }),

/***/ "./resources/js/components/PriceListItemsFormRow.vue?vue&type=template&id=55edac3c&scoped=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/components/PriceListItemsFormRow.vue?vue&type=template&id=55edac3c&scoped=true& ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsFormRow_vue_vue_type_template_id_55edac3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsFormRow_vue_vue_type_template_id_55edac3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceListItemsFormRow_vue_vue_type_template_id_55edac3c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PriceListItemsFormRow.vue?vue&type=template&id=55edac3c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsFormRow.vue?vue&type=template&id=55edac3c&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/AddNailsWindow.vue?vue&type=template&id=12623d37&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/AddNailsWindow.vue?vue&type=template&id=12623d37&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "modal",
    {
      on: {
        "modal-close": function($event) {
          return _vm.close()
        }
      }
    },
    [
      _c(
        "div",
        {
          staticClass: "bg-white rounded-lg shadow-lg overflow-hidden",
          staticStyle: { width: "900px" }
        },
        [
          _c(
            "div",
            { staticClass: "p-8" },
            [
              _c(
                "heading",
                {
                  staticClass: "mb-6 text-90 font-normal text-xl",
                  attrs: { level: 2 }
                },
                [_vm._v("Pridanie klincov do cenníku")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "text-100 container" }, [
                _c("div", { staticClass: "flex bg-30 p-2 rounded-lg" }, [
                  _c("div", { staticClass: "flex-1" }, [
                    _c("label", { staticClass: "font-bold" }, [
                      _vm._v(
                        "\n                                Priemer\n                                "
                      ),
                      _c(
                        "button",
                        {
                          staticClass:
                            "rounded p-1 text-80 hover:text-20 hover:bg-70",
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              return _vm.selectAllWires()
                            }
                          }
                        },
                        [
                          _c("i", {
                            staticClass: "far fa-check-square font-normal"
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c("span", [_vm._v("/")]),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass:
                            "rounded p-1 text-80 hover:text-20 hover:bg-70",
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              return _vm.deselectAllWires()
                            }
                          }
                        },
                        [_c("i", { staticClass: "far fa-square font-normal" })]
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "flex flex-wrap" },
                      _vm._l(_vm.nailWires, function(wire) {
                        return _c(
                          "div",
                          {
                            key: wire.id,
                            staticClass: "m-2",
                            staticStyle: { "white-space": "nowrap" }
                          },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.selectedWires,
                                  expression: "selectedWires"
                                }
                              ],
                              staticClass: "mr-2",
                              attrs: { type: "checkbox" },
                              domProps: {
                                value: wire.id,
                                checked: Array.isArray(_vm.selectedWires)
                                  ? _vm._i(_vm.selectedWires, wire.id) > -1
                                  : _vm.selectedWires
                              },
                              on: {
                                change: function($event) {
                                  var $$a = _vm.selectedWires,
                                    $$el = $event.target,
                                    $$c = $$el.checked ? true : false
                                  if (Array.isArray($$a)) {
                                    var $$v = wire.id,
                                      $$i = _vm._i($$a, $$v)
                                    if ($$el.checked) {
                                      $$i < 0 &&
                                        (_vm.selectedWires = $$a.concat([$$v]))
                                    } else {
                                      $$i > -1 &&
                                        (_vm.selectedWires = $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1)))
                                    }
                                  } else {
                                    _vm.selectedWires = $$c
                                  }
                                }
                              }
                            }),
                            _vm._v(
                              _vm._s(wire.name) +
                                "\n                                "
                            )
                          ]
                        )
                      }),
                      0
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "flex-1" }, [
                    _c("label", { staticClass: "font-bold" }, [
                      _vm._v("Povrchová úprava")
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "flex flex-wrap" },
                      [
                        _c("div", { staticClass: "m-2" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.selectedFinishes,
                                expression: "selectedFinishes"
                              }
                            ],
                            staticClass: "mr-2",
                            attrs: { type: "checkbox", value: "0" },
                            domProps: {
                              checked: Array.isArray(_vm.selectedFinishes)
                                ? _vm._i(_vm.selectedFinishes, "0") > -1
                                : _vm.selectedFinishes
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.selectedFinishes,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = "0",
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.selectedFinishes = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.selectedFinishes = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.selectedFinishes = $$c
                                }
                              }
                            }
                          }),
                          _vm._v(
                            " bez úpravy\n                                "
                          )
                        ]),
                        _vm._v(" "),
                        _vm._l(_vm.nailFinishies, function(finish) {
                          return _c(
                            "div",
                            { key: finish.id, staticClass: "m-2" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.selectedFinishes,
                                    expression: "selectedFinishes"
                                  }
                                ],
                                staticClass: "mr-2",
                                attrs: { type: "checkbox" },
                                domProps: {
                                  value: finish.id,
                                  checked: Array.isArray(_vm.selectedFinishes)
                                    ? _vm._i(_vm.selectedFinishes, finish.id) >
                                      -1
                                    : _vm.selectedFinishes
                                },
                                on: {
                                  change: function($event) {
                                    var $$a = _vm.selectedFinishes,
                                      $$el = $event.target,
                                      $$c = $$el.checked ? true : false
                                    if (Array.isArray($$a)) {
                                      var $$v = finish.id,
                                        $$i = _vm._i($$a, $$v)
                                      if ($$el.checked) {
                                        $$i < 0 &&
                                          (_vm.selectedFinishes = $$a.concat([
                                            $$v
                                          ]))
                                      } else {
                                        $$i > -1 &&
                                          (_vm.selectedFinishes = $$a
                                            .slice(0, $$i)
                                            .concat($$a.slice($$i + 1)))
                                      }
                                    } else {
                                      _vm.selectedFinishes = $$c
                                    }
                                  }
                                }
                              }),
                              _vm._v(
                                _vm._s(finish.name) +
                                  "\n                                "
                              )
                            ]
                          )
                        })
                      ],
                      2
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "container p-2 bg-30 mt-2 rounded-t-lg" },
                  [
                    _c(
                      "button",
                      {
                        staticClass:
                          "rounded p-1 text-80 hover:text-20 hover:bg-70",
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.selectAllNails()
                          }
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "far fa-check-square text-xl font-normal"
                        })
                      ]
                    ),
                    _vm._v(" "),
                    _c("span", [_vm._v("/")]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass:
                          "rounded p-1 text-80 hover:text-20 hover:bg-70",
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.deselectAllNails()
                          }
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "far fa-square text-xl font-normal"
                        })
                      ]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticStyle: {
                      "max-height": "400px",
                      "overflow-y": "scroll"
                    }
                  },
                  _vm._l(_vm.nailsFiltered, function(nail, index) {
                    return _c(
                      "div",
                      {
                        key: nail.id,
                        staticClass: "flex hover:bg-20 cursor-pointer",
                        on: {
                          click: function($event) {
                            return _vm.checkNail(nail.id)
                          }
                        }
                      },
                      [
                        _c("div", { staticClass: "p-2" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.selectedNails,
                                expression: "selectedNails"
                              }
                            ],
                            attrs: { type: "checkbox" },
                            domProps: {
                              value: nail.id,
                              checked: Array.isArray(_vm.selectedNails)
                                ? _vm._i(_vm.selectedNails, nail.id) > -1
                                : _vm.selectedNails
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.selectedNails,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = nail.id,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.selectedNails = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.selectedNails = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.selectedNails = $$c
                                }
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "flex-1 p-2" }, [
                          _vm._v(
                            _vm._s(nail.wire.average) +
                              " x " +
                              _vm._s(nail.length)
                          )
                        ])
                      ]
                    )
                  }),
                  0
                )
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "bg-30 px-6 py-3 flex" }, [
            _c("div", { staticClass: "flex items-center ml-auto" }, [
              _c(
                "button",
                {
                  staticClass: "btn text-80 font-normal h-9 px-3 mr-3 btn-link",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.close()
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                        Zrušiť\n                    "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-default btn-danger",
                  attrs: { type: "button" },
                  on: {
                    click: function($event) {
                      return _vm.addNailsToPriceList()
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                        Pridať\n                    "
                  )
                ]
              )
            ])
          ])
        ]
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsForm.vue?vue&type=template&id=470f9a08&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsForm.vue?vue&type=template&id=470f9a08& ***!
  \******************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "card pt-2 mb-4" }, [
        _c("div", { staticClass: "p-4 pb-4 flex" }, [
          _c("div", [
            _c("div", { staticClass: "mb-4" }, [
              _c("div", { staticClass: "flex mb-2" }, [
                _c("div", { staticClass: "font-normal text-80 w-1/4" }, [
                  _vm._v("ID")
                ]),
                _c("div", [_vm._v(_vm._s(_vm.priceListData.id))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex mb-2" }, [
                _c("div", { staticClass: "font-normal text-80 w-1/4" }, [
                  _vm._v("Klient")
                ]),
                _c("div", [_vm._v(_vm._s(_vm.priceListData.client.name))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex mb-2" }, [
                _c("div", { staticClass: "font-normal text-80 w-1/4" }, [
                  _vm._v("Typ cenníka")
                ]),
                _c("div", [_vm._v(_vm._s(_vm.priceListType))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "flex mb-2" }, [
                _c("div", { staticClass: "font-normal text-80 w-1/4" }, [
                  _vm._v("Vytvorené")
                ]),
                _c("div", [_vm._v(_vm._s(_vm.createdAt))])
              ])
            ]),
            _vm._v(" "),
            !_vm.readonly
              ? _c(
                  "button",
                  {
                    staticClass: "btn btn-default btn-primary",
                    on: {
                      click: function($event) {
                        return _vm.openAddNailWindow()
                      }
                    }
                  },
                  [_vm._v("Pridať klince")]
                )
              : _vm._e(),
            _vm._v(" "),
            !_vm.readonly
              ? _c(
                  "button",
                  {
                    staticClass: "btn btn-default btn-primary",
                    on: {
                      click: function($event) {
                        return _vm.savePriceListItems()
                      }
                    }
                  },
                  [_vm._v("Uložiť parametre")]
                )
              : _vm._e(),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-default btn-primary",
                on: {
                  click: function($event) {
                    return _vm.copyPriceList()
                  }
                }
              },
              [_vm._v("Kopírovať cenník")]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "pr-8 pl-8 flex-1" }, [
            _c("div", { staticClass: "font-normal text-80" }, [
              _vm._v("Poznámky")
            ]),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.notes,
                  expression: "notes"
                }
              ],
              staticClass: " form-input-bordered",
              attrs: { cols: "60", rows: "6" },
              domProps: { value: _vm.notes },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.notes = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", {}, [
            _c(
              "button",
              {
                staticClass: "btn btn-default btn-primary",
                on: {
                  click: function($event) {
                    return _vm.savePriceListAndExport()
                  }
                }
              },
              [_vm._v("Export PDF")]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "p-2" }, [
              _c("div", { staticClass: "mt-2" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.pdfShowNumberPerCarton,
                      expression: "pdfShowNumberPerCarton"
                    }
                  ],
                  attrs: { type: "checkbox" },
                  domProps: {
                    checked: Array.isArray(_vm.pdfShowNumberPerCarton)
                      ? _vm._i(_vm.pdfShowNumberPerCarton, null) > -1
                      : _vm.pdfShowNumberPerCarton
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.pdfShowNumberPerCarton,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            (_vm.pdfShowNumberPerCarton = $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            (_vm.pdfShowNumberPerCarton = $$a
                              .slice(0, $$i)
                              .concat($$a.slice($$i + 1)))
                        }
                      } else {
                        _vm.pdfShowNumberPerCarton = $$c
                      }
                    }
                  }
                }),
                _vm._v(" Počet v kartóne")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "mt-2" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.pdfShowNumberPerPallet,
                      expression: "pdfShowNumberPerPallet"
                    }
                  ],
                  attrs: { type: "checkbox" },
                  domProps: {
                    checked: Array.isArray(_vm.pdfShowNumberPerPallet)
                      ? _vm._i(_vm.pdfShowNumberPerPallet, null) > -1
                      : _vm.pdfShowNumberPerPallet
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.pdfShowNumberPerPallet,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            (_vm.pdfShowNumberPerPallet = $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            (_vm.pdfShowNumberPerPallet = $$a
                              .slice(0, $$i)
                              .concat($$a.slice($$i + 1)))
                        }
                      } else {
                        _vm.pdfShowNumberPerPallet = $$c
                      }
                    }
                  }
                }),
                _vm._v(" Počet na palete")
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "card pt-2" }, [
        _c("div", { staticClass: "py-3 px-3" }, [
          _c(
            "div",
            { staticClass: "flex flex-wrap" },
            _vm._l(_vm.panel.fields[0].nailWires, function(wire) {
              return _c(
                "div",
                {
                  key: wire.id,
                  staticClass: "m-2",
                  staticStyle: { "white-space": "nowrap" }
                },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.filterSelectedWires,
                        expression: "filterSelectedWires"
                      }
                    ],
                    staticClass: "mr-2",
                    attrs: { type: "checkbox" },
                    domProps: {
                      value: wire.id,
                      checked: Array.isArray(_vm.filterSelectedWires)
                        ? _vm._i(_vm.filterSelectedWires, wire.id) > -1
                        : _vm.filterSelectedWires
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.filterSelectedWires,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = wire.id,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              (_vm.filterSelectedWires = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.filterSelectedWires = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.filterSelectedWires = $$c
                        }
                      }
                    }
                  }),
                  _vm._v(_vm._s(wire.name) + "\n        ")
                ]
              )
            }),
            0
          )
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "flex items-center py-3 border-b border-50" },
          [
            _c("div", { staticClass: "flex items-center" }, [
              _c("div", { staticClass: "px-3" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.selectAllRows,
                      expression: "selectAllRows"
                    }
                  ],
                  staticClass: "checkbox",
                  attrs: { type: "checkbox" },
                  domProps: {
                    checked: Array.isArray(_vm.selectAllRows)
                      ? _vm._i(_vm.selectAllRows, null) > -1
                      : _vm.selectAllRows
                  },
                  on: {
                    click: function($event) {
                      _vm.selectAllRows = !_vm.selectAllRows
                    },
                    change: function($event) {
                      var $$a = _vm.selectAllRows,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 && (_vm.selectAllRows = $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            (_vm.selectAllRows = $$a
                              .slice(0, $$i)
                              .concat($$a.slice($$i + 1)))
                        }
                      } else {
                        _vm.selectAllRows = $$c
                      }
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "flex items-center ml-auto px-3" }, [
              _c("div", [
                _c("div", { staticClass: "flex items-center mr-3" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.actionType,
                          expression: "actionType"
                        }
                      ],
                      staticClass: "form-control form-select mr-2",
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.actionType = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        }
                      }
                    },
                    [
                      _c(
                        "option",
                        {
                          attrs: {
                            value: "",
                            disabled: "disabled",
                            selected: "selected"
                          }
                        },
                        [_vm._v("Hromadná zmena")]
                      ),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "koeficient" } }, [
                        _vm._v("Cenový koeficient")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "prirazka" } }, [
                        _vm._v("Prirážka")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass:
                        "btn btn-default btn-primary\n                flex\n                items-center\n                justify-center\n                px-3",
                      attrs: {
                        "data-testid": "action-confirm",
                        dusk: "run-action-button",
                        title: "Vykonať akciu"
                      },
                      on: {
                        click: function($event) {
                          return _vm.openActionDialog()
                        }
                      }
                    },
                    [
                      _c(
                        "svg",
                        {
                          staticClass: "fill-current text-white",
                          staticStyle: { "margin-left": "7px" },
                          attrs: {
                            xmlns: "http://www.w3.org/2000/svg",
                            width: "20",
                            height: "20",
                            viewBox: "0 0 20 20",
                            "aria-labelledby": "play",
                            role: "presentation"
                          }
                        },
                        [
                          _c("path", {
                            attrs: {
                              "fill-rule": "nonzero",
                              d:
                                "M0 .213l15.925 9.77L0 19.79V.213zm2 3.574V16.21l10.106-6.224L2 3.786z"
                            }
                          })
                        ]
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("div", {
                  staticClass: "v-portal",
                  staticStyle: { display: "none" },
                  attrs: { transition: "fade-transition" }
                })
              ]),
              _vm._v(" "),
              _c("div", { attrs: { dusk: "delete-menu" } }, [
                _c(
                  "div",
                  { staticClass: "v-popover ml-3 bg-30 hover:bg-40 rounded" },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "trigger",
                        staticStyle: { display: "inline-block" }
                      },
                      [
                        _c(
                          "button",
                          {
                            staticClass:
                              "\n                  px-3\n                  py-2\n                  rounded\n                  active:outline-none\n                  active:shadow-outline\n                  focus:outline-none\n                  focus:shadow-outline\n                ",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                return _vm.openMultiDeleteDialog()
                              }
                            }
                          },
                          [
                            _c(
                              "svg",
                              {
                                staticClass: "fill-current text-80",
                                attrs: {
                                  xmlns: "http://www.w3.org/2000/svg",
                                  width: "20",
                                  height: "20",
                                  viewBox: "0 0 20 20",
                                  "aria-labelledby": "delete",
                                  role: "presentation"
                                }
                              },
                              [
                                _c("path", {
                                  attrs: {
                                    "fill-rule": "nonzero",
                                    d:
                                      "M6 4V2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2h5a1 1 0 0 1 0 2h-1v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6H1a1 1 0 1 1 0-2h5zM4 6v12h12V6H4zm8-2V2H8v2h4zM8 8a1 1 0 0 1 1 1v6a1 1 0 0 1-2 0V9a1 1 0 0 1 1-1zm4 0a1 1 0 0 1 1 1v6a1 1 0 0 1-2 0V9a1 1 0 0 1 1-1z"
                                  }
                                })
                              ]
                            )
                          ]
                        )
                      ]
                    )
                  ]
                )
              ])
            ])
          ]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "relative" }, [
          _c(
            "div",
            { staticClass: "overflow-hidden overflow-x-auto relative" },
            [
              _c(
                "table",
                {
                  staticClass: "table w-full table-default",
                  attrs: {
                    cellpadding: "0",
                    cellspacing: "0",
                    "data-testid": "resource-table"
                  }
                },
                [
                  _c("thead", [
                    _c(
                      "tr",
                      [
                        _c("th", { staticClass: "w-16" }, [_vm._v(" ")]),
                        _vm._v(" "),
                        _vm._l(_vm.priceListItemsHeader, function(
                          headerItem,
                          index
                        ) {
                          return _c(
                            "th",
                            { key: index, staticClass: "text-left" },
                            [
                              _vm._v(
                                "\n                  " +
                                  _vm._s(headerItem.title) +
                                  "\n              "
                              )
                            ]
                          )
                        }),
                        _vm._v(" "),
                        _c("th", [_vm._v(" ")])
                      ],
                      2
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "tbody",
                    _vm._l(_vm.priceListItems, function(priceListItem, index) {
                      return _c("price-list-items-form-row", {
                        key: priceListItem.id,
                        attrs: {
                          readonly: _vm.readonly,
                          constants: _vm.panel.fields[0].constants,
                          priceListType: _vm.priceListData.type
                        },
                        on: {
                          "delete-row": function($event) {
                            return _vm.loadPriceListItems()
                          }
                        },
                        model: {
                          value: _vm.priceListItems[index],
                          callback: function($$v) {
                            _vm.$set(_vm.priceListItems, index, $$v)
                          },
                          expression: "priceListItems[index]"
                        }
                      })
                    }),
                    1
                  )
                ]
              )
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _vm.showAddNailWindow
        ? _c("add-nails-window", {
            attrs: {
              nailWires: _vm.panel.fields[0].nailWires,
              nailFinishies: _vm.panel.fields[0].nailFinishies,
              priceListId: _vm.resourceId
            },
            on: {
              close: function($event) {
                return _vm.closeAddNailWindow($event)
              }
            }
          })
        : _vm._e(),
      _vm._v(" "),
      _vm.multiDeleteDialogShow
        ? _c(
            "modal",
            {
              on: {
                "modal-close": function($event) {
                  return _vm.closeMultiDeleteDialog()
                }
              }
            },
            [
              _c(
                "div",
                {
                  staticClass:
                    "bg-white rounded-lg shadow-lg overflow-hidden text-left"
                },
                [
                  _c(
                    "div",
                    { staticClass: "p-8" },
                    [
                      _c(
                        "heading",
                        {
                          staticClass: "mb-6 text-90 font-normal text-xl",
                          attrs: { level: 2 }
                        },
                        [_vm._v("Zmazanie záznamov z cenníka")]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "text-100 container" }, [
                        _vm._v(
                          "\n                    Naozaj chcete zmazať všetky vybrané položky z cenníka?\n                "
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "bg-30 px-6 py-3 flex" }, [
                    _c("div", { staticClass: "flex items-center ml-auto" }, [
                      _c(
                        "button",
                        {
                          staticClass:
                            "btn text-80 font-normal h-9 px-3 mr-3 btn-link",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              return _vm.closeMultiDeleteDialog()
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                        Zrušiť\n                    "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default btn-danger",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              return _vm.deleteSelectedItems()
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                        Zmazať\n                    "
                          )
                        ]
                      )
                    ])
                  ])
                ]
              )
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.actionDialogShow
        ? _c(
            "modal",
            {
              on: {
                "modal-close": function($event) {
                  return _vm.closeActionDialog()
                }
              }
            },
            [
              _c(
                "div",
                {
                  staticClass:
                    "bg-white rounded-lg shadow-lg overflow-hidden text-left"
                },
                [
                  _c(
                    "div",
                    { staticClass: "p-8" },
                    [
                      _c(
                        "heading",
                        {
                          staticClass: "mb-6 text-90 font-normal text-xl",
                          attrs: { level: 2 }
                        },
                        [_vm._v("Hromadná úprava")]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.actionType == "koeficient",
                              expression: "actionType=='koeficient'"
                            }
                          ],
                          staticClass: "text-100 container"
                        },
                        [
                          _vm._v("\n                    Cenový koeficient: "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.actionValue,
                                expression: "actionValue"
                              }
                            ],
                            staticClass:
                              "form-control form-input form-input-bordered w-40",
                            attrs: { type: "number", step: "0.01", min: "0" },
                            domProps: { value: _vm.actionValue },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.actionValue = $event.target.value
                              }
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.actionType == "prirazka",
                              expression: "actionType=='prirazka'"
                            }
                          ],
                          staticClass: "text-100 container"
                        },
                        [
                          _vm._v("\n                    Prirážka: "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.actionValue,
                                expression: "actionValue"
                              }
                            ],
                            staticClass:
                              "form-control form-input form-input-bordered w-40",
                            attrs: { type: "number", step: "1", min: "0" },
                            domProps: { value: _vm.actionValue },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.actionValue = $event.target.value
                              }
                            }
                          })
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "bg-30 px-6 py-3 flex" }, [
                    _c("div", { staticClass: "flex items-center ml-auto" }, [
                      _c(
                        "button",
                        {
                          staticClass:
                            "btn text-80 font-normal h-9 px-3 mr-3 btn-link",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              return _vm.closeActionDialog()
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                        Zrušiť\n                    "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default btn-primary",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              return _vm.doAction()
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n                        Vykonať akciu\n                    "
                          )
                        ]
                      )
                    ])
                  ])
                ]
              )
            ]
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsFormRow.vue?vue&type=template&id=55edac3c&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/PriceListItemsFormRow.vue?vue&type=template&id=55edac3c&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("tr", [
    _c("td", { staticClass: "w-16" }, [
      _c("input", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.priceListItem.selected,
            expression: "priceListItem.selected"
          }
        ],
        staticClass: "checkbox",
        attrs: { type: "checkbox" },
        domProps: {
          value: _vm.priceListItem.id,
          checked: Array.isArray(_vm.priceListItem.selected)
            ? _vm._i(_vm.priceListItem.selected, _vm.priceListItem.id) > -1
            : _vm.priceListItem.selected
        },
        on: {
          change: function($event) {
            var $$a = _vm.priceListItem.selected,
              $$el = $event.target,
              $$c = $$el.checked ? true : false
            if (Array.isArray($$a)) {
              var $$v = _vm.priceListItem.id,
                $$i = _vm._i($$a, $$v)
              if ($$el.checked) {
                $$i < 0 &&
                  _vm.$set(_vm.priceListItem, "selected", $$a.concat([$$v]))
              } else {
                $$i > -1 &&
                  _vm.$set(
                    _vm.priceListItem,
                    "selected",
                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                  )
              }
            } else {
              _vm.$set(_vm.priceListItem, "selected", $$c)
            }
          }
        }
      })
    ]),
    _vm._v(" "),
    _c("td", [
      _c("div", { staticClass: "text-left" }, [
        _c("span", { staticClass: "whitespace-no-wrap" }, [
          _vm._v(
            "\n                " +
              _vm._s(_vm.priceListItemName) +
              "\n            "
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("td", [
      _c("div", { staticClass: "text-left" }, [
        _c("span", { staticClass: "whitespace-no-wrap" }, [
          _vm._v(
            "\n                " +
              _vm._s(_vm.priceListItem.nail_weight_1000) +
              "\n            "
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("td", [
      _c("div", { staticClass: "text-left" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.priceListItem.wire_price,
              expression: "priceListItem.wire_price"
            }
          ],
          staticClass: "form-control form-input form-input-bordered w-40",
          attrs: {
            type: "number",
            step: "1",
            min: "0",
            readonly: _vm.readonly
          },
          domProps: { value: _vm.priceListItem.wire_price },
          on: {
            input: [
              function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.priceListItem, "wire_price", $event.target.value)
              },
              function($event) {
                return _vm.$emit("input", _vm.priceListItem)
              }
            ]
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c(
      "td",
      {
        directives: [
          {
            name: "show",
            rawName: "v-show",
            value: _vm.priceListType == "welded",
            expression: "priceListType=='welded'"
          }
        ]
      },
      [
        _c("div", { staticClass: "text-left" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.priceListItem.nail_price_coefficient,
                expression: "priceListItem.nail_price_coefficient"
              }
            ],
            staticClass: "form-control form-input form-input-bordered w-40",
            attrs: {
              type: "number",
              step: "0.01",
              min: "0",
              readonly: _vm.readonly
            },
            domProps: { value: _vm.priceListItem.nail_price_coefficient },
            on: {
              input: [
                function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.priceListItem,
                    "nail_price_coefficient",
                    $event.target.value
                  )
                },
                function($event) {
                  return _vm.$emit("input", _vm.priceListItem)
                }
              ]
            }
          })
        ])
      ]
    ),
    _vm._v(" "),
    _c(
      "td",
      {
        directives: [
          {
            name: "show",
            rawName: "v-show",
            value: _vm.priceListType == "loose",
            expression: "priceListType=='loose'"
          }
        ]
      },
      [
        _c("div", { staticClass: "text-left" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.priceListItem.nail_price_per_kg_coefficient,
                expression: "priceListItem.nail_price_per_kg_coefficient"
              }
            ],
            staticClass: "form-control form-input form-input-bordered w-40",
            attrs: {
              type: "number",
              step: "0.01",
              min: "0",
              readonly: _vm.readonly
            },
            domProps: {
              value: _vm.priceListItem.nail_price_per_kg_coefficient
            },
            on: {
              input: [
                function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.priceListItem,
                    "nail_price_per_kg_coefficient",
                    $event.target.value
                  )
                },
                function($event) {
                  return _vm.$emit("input", _vm.priceListItem)
                }
              ]
            }
          })
        ])
      ]
    ),
    _vm._v(" "),
    _c("td", [
      _c("div", { staticClass: "text-left" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.priceListItem.surcharge,
              expression: "priceListItem.surcharge"
            }
          ],
          staticClass: "form-control form-input form-input-bordered w-40",
          attrs: {
            type: "number",
            step: "1",
            min: "0",
            readonly: _vm.readonly
          },
          domProps: { value: _vm.priceListItem.surcharge },
          on: {
            input: [
              function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.priceListItem, "surcharge", $event.target.value)
              },
              function($event) {
                return _vm.$emit("input", _vm.priceListItem)
              }
            ]
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c(
      "td",
      {
        directives: [
          {
            name: "show",
            rawName: "v-show",
            value: _vm.priceListType == "welded",
            expression: "priceListType=='welded'"
          }
        ]
      },
      [
        _c("div", { staticClass: "text-left" }, [
          _c("input", {
            staticClass: "form-control form-input form-input-bordered w-40",
            attrs: {
              type: "number",
              step: "0.0001",
              readonly: "readonly",
              min: "0"
            },
            domProps: { value: _vm.price_1000 },
            on: {
              input: function($event) {
                return _vm.$emit("input", _vm.priceListItem)
              }
            }
          })
        ])
      ]
    ),
    _vm._v(" "),
    _c(
      "td",
      {
        directives: [
          {
            name: "show",
            rawName: "v-show",
            value: _vm.priceListType == "loose",
            expression: "priceListType=='loose'"
          }
        ]
      },
      [
        _c("div", { staticClass: "text-left" }, [
          _c("input", {
            staticClass: "form-control form-input form-input-bordered w-40",
            attrs: {
              type: "number",
              step: "0.0001",
              readonly: "readonly",
              min: "0"
            },
            domProps: { value: _vm.price_per_kg },
            on: {
              input: function($event) {
                return _vm.$emit("input", _vm.priceListItem)
              }
            }
          })
        ])
      ]
    ),
    _vm._v(" "),
    _c("td", { staticClass: "td-fit text-right pr-6 align-middle" }, [
      !_vm.readonly
        ? _c(
            "div",
            { staticClass: "inline-flex items-center" },
            [
              _c(
                "button",
                {
                  staticClass:
                    "\n                    inline-flex\n                    appearance-none\n                    cursor-pointer\n                    text-70\n                    hover:text-primary\n                    mr-3\n                    has-tooltip",
                  on: {
                    click: function($event) {
                      return _vm.openDeleteDialog()
                    }
                  }
                },
                [
                  _c(
                    "svg",
                    {
                      staticClass: "fill-current",
                      attrs: {
                        xmlns: "http://www.w3.org/2000/svg",
                        width: "20",
                        height: "20",
                        viewBox: "0 0 20 20",
                        "aria-labelledby": "delete",
                        role: "presentation"
                      }
                    },
                    [
                      _c("path", {
                        attrs: {
                          "fill-rule": "nonzero",
                          d:
                            "M6 4V2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2h5a1 1 0 0 1 0 2h-1v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6H1a1 1 0 1 1 0-2h5zM4 6v12h12V6H4zm8-2V2H8v2h4zM8 8a1 1 0 0 1 1 1v6a1 1 0 0 1-2 0V9a1 1 0 0 1 1-1zm4 0a1 1 0 0 1 1 1v6a1 1 0 0 1-2 0V9a1 1 0 0 1 1-1z"
                        }
                      })
                    ]
                  )
                ]
              ),
              _vm._v(" "),
              _vm.deleteDialogShow
                ? _c(
                    "modal",
                    {
                      on: {
                        "modal-close": function($event) {
                          return _vm.closeDeleteDialog()
                        }
                      }
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass:
                            "bg-white rounded-lg shadow-lg overflow-hidden text-left"
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "p-8" },
                            [
                              _c(
                                "heading",
                                {
                                  staticClass:
                                    "mb-6 text-90 font-normal text-xl",
                                  attrs: { level: 2 }
                                },
                                [_vm._v("Zmazanie záznamu z cenníka")]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "text-100 container" }, [
                                _vm._v(
                                  "\n                                Naozaj chcete zmazať položku "
                                ),
                                _c("b", [
                                  _vm._v(_vm._s(_vm.priceListItemName))
                                ]),
                                _vm._v(
                                  " z cenníka?\n                            "
                                )
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "bg-30 px-6 py-3 flex" }, [
                            _c(
                              "div",
                              { staticClass: "flex items-center ml-auto" },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "btn text-80 font-normal h-9 px-3 mr-3 btn-link",
                                    attrs: { type: "button" },
                                    on: {
                                      click: function($event) {
                                        return _vm.closeDeleteDialog()
                                      }
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                                    Zrušiť\n                                "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-default btn-danger",
                                    attrs: { type: "button" },
                                    on: {
                                      click: function($event) {
                                        return _vm.deleteItem()
                                      }
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                                    Zmazať\n                                "
                                    )
                                  ]
                                )
                              ]
                            )
                          ])
                        ]
                      )
                    ]
                  )
                : _vm._e()
            ],
            1
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ normalizeComponent)
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/tool": 0,
/******/ 			"css/tool": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/tool"], () => (__webpack_require__("./resources/js/tool.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/tool"], () => (__webpack_require__("./resources/sass/tool.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;