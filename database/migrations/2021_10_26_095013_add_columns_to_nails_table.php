<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToNailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nails', function (Blueprint $table) {
            $table->double('number_per_carton')->default(0);
            $table->double('number_per_pallet')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nails', function (Blueprint $table) {
            $table->dropColumn(['number_per_carton','number_per_pallet']);
        });
    }
}
