<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Kovex - cenník</title>
<style>
    @page {
        margin: 160px 0px 70px 0px;
        background-color: #fdfdfd;
    }

    *{ font-family: DejaVu Sans; font-size: 12px;}

    header {
        position: fixed;
        top: -125px;
        left: 0px;
        right: 0px;


        /** Extra personal styles **/
        background-color: #03a9f4;
        color: white;
        text-align: center;
        line-height: 35px;

    }

    footer {
        position: fixed;
        bottom: 0px;
        left: 0px;
        right: 0px;
        height: 0px;

        /** Extra personal styles **/
        text-align: center;
        letter-spacing: -0.5pt;
    }

    footer .page:after { content: counter(page); }

    .footer-content {
        border-top: 2px solid #da2127;
        margin-left: 25px;
        margin-right: 25px;
    }

    .text-s {
        font-size: 11px;
    }
    .text-l {
        font-size: 13px;
    }
    .text-xl {
        font-size: 14px;
    }
    .text-red {
        color: #da2127;
    }

    .text-gray {
        color: #6e6f71;
    }

    .text-white {
        color: #FFFFFF;
    }

    .bg-red {
        background-color: #da2127;
    }
    .bg-gray {
        background-repeat: #6e6f71;
    }

    .text-bold {
        font-weight: bold;
    }

    .text-right {
        text-align: right;
    }

    .sep {
        color: #da2127;
        font-weight: bold;
        margin: 0 2px;
    }
    .bg-page {
        width: 100%;
        height: 100%;
        position: absolute;

        background-image: url("{{  storage_path('app/pdf_template/images') }}/page_bg.png");
        background-position: 0 0;
        background-repeat: no-repeat;
        background-size: cover;
        z-index: 1;
    }

    .header-content {
        z-index: 2;
        width: 100%;
        margin: 0px 25px 0px 25px;
    }
    .header-content .row-1 td {
        height: 80px;
    }
    .header-content .logo {
        background: url("{{  storage_path('app/pdf_template/images') }}/header_bg.png");
        background-size: contain;
        background-repeat: no-repeat;
    }
    .header-content .row-2 td{
        vertical-align: top;
        line-height: 20px;
        padding: 0px;
        margin: 0px;
    }
    .header-content .row-2 .td-gap {
        width: 270px;
    }
    .header-content .row-2 .text-info {
        letter-spacing: -0.5pt;
    }
    .header-content .row-2 .link {
        width: 100px;
    }

    .content {
        z-index: 2;
        margin: 0px 25px 25px 25px;
    }

    .client {
        float: right;

    }

    .clear {
        clear: both;
    }

    .product-title {
        padding: 5px;
    }

    .price-table {
        margin-top: 20px;
    }
    .price-table th{
        padding: 6px;
        border: 1px solid #6e6f71;
        background-color: #f4f4f4;
    }
    .price-table td{
        padding: 6px;
        border: 1px solid #6e6f71;
    }

    .ml-1 {
        margin-left: 10px;
    }

    .gen-info {
        float: right;
    }
</style>
</head>
<body>
    <header>
        <div class="bg-page"></div>
        <table class="header-content" cellspacing="0" cellpadding="0">
            <tr class="row-1">
                <td colspan="3" class="logo" style="text-align: right;padding: 0px;">
                    <div style="color: white;font-weight: bold;padding: 0px 10px;margin:0px;">
                        <div style="padding: 0px;margin:0px;">
                            <div style="padding: 0px 0px 0px 10px;margin:0px;line-height: 12px;float: right;">+421 911 695 596</div>
                            <div style="padding: 0px;margin:0px;line-height: 12px;float: right;">marcel.balvan@kovex-hlohovec.sk</div>
                            <div style="clear: both;"></div>
                        </div>
                        <div style="padding: 5px 0px 0px 0px;margin:0px;">
                            <div style="padding: 0px 0px 0px 10px;margin:0px;line-height: 12px;float: right;">+421 903 248 167</div>
                            <div style="padding: 0px;margin:0px;line-height: 12px;float: right;">info@kovex-hlohovec.sk</div>
                            <div style="clear: both;"></div>
                        </div>

                    </div>
                </td>
            </tr>
            <tr class="row-2">
                <td class="td-gap">&nbsp;</td>
                <td class="text-info"><span class="text-gray text-bold">Kovex s.r.o., Šulekovo 1212, 92003 Šulekovo</span></td>
                <td class="text-right link"><span class="text-red text-bold">www.klince.com</span></td>
            </tr>
        </table>

    </header>
    <footer>
        <div class="text-s footer-content">
            <span class="text-red text-bold">FAKTURAČNÁ ADRESA:</span>
            <span class="text-gray">Kovex s.r.o., Šulekovo 1212, 92003 Šulekovo</span>
            <span class="sep">|</span>
            <span class="text-red text-bold">IČO:</span>
            <span class="text-gray">314 24 201</span>
            <span class="sep">|</span>
            <span class="text-red text-bold">DIČ:</span>
            <span class="text-gray">2020396543</span>
            <span class="sep">|</span>
            <span class="text-red text-bold">IČDPH:</span>
            <span class="text-gray">SK2020396543</span>
        </div>
         <p class="page"></p>
    </footer>

    <main class="content">

        <div class="client">
            <div class="text-l">{{ $data->client->name }}</div>
            <div class="text-l">{{ $data->client->street }} {{ $data->client->street_number }}</div>
            <div class="text-l">{{ $data->client->city }} {{ $data->client->zip }}</div>
        </div>

        <div class="clear"></div>
        <br>

        @if ( $data->type == 'welded')
            <div class="product-title bg-red text-white text-bold text-xl">Klince do pneumatických klincovačiek</div>
        @endif

        @if ( $data->type == 'loose')
            <div class="product-title bg-red text-white text-bold text-xl">Voľne sypané klince</div>
        @endif

        <table class="price-table ml-1" cellspacing="0" cellpadding="0">
            <tr>
                <th>Rozmer klincov</th>
                @if ( $data->pdf_show_number_per_carton )
                <th>Počet v kartóne</th>
                @endif
                @if ( $data->pdf_show_number_per_pallet )
                <th>Počet na palete</th>
                @endif
                @if ( $data->type == 'welded')
                    <th>Cena za 1000 ks <br>bez DPH [&euro;]</th>
                @endif
                @if ( $data->type == 'loose')
                    <th>Cena za kg <br>bez DPH [&euro;]</th>
                @endif

            </tr>
            @foreach($data->priceListItems as $item)
            <tr>
                <td>{{ $item->wire_average }}x{{ $item->nail_length }} @if($item->nail_finish_id>0) {{ $item->nail_finish_label }} @else Bk @endif</td>
                @if ( $data->pdf_show_number_per_carton )
                <td>{{ number_format($item->nail_number_per_carton, 0, ".", " ") }}</td>
                @endif
                @if ( $data->pdf_show_number_per_pallet )
                <td>{{ number_format($item->nail_number_per_pallet, 0, ".", " ") }}</td>
                @endif
                @if ( $data->type == 'welded')
                    <td class="text-right">{{ $item->price_1000 }}</td>
                @endif
                @if ( $data->type == 'loose')
                    <td class="text-right">{{ $item->price_per_kg }}</td>
                @endif

            </tr>
            @endforeach
        </table>

        <br>
        <br>
        @if( trim($data->notes)!='' )
        <div class="product-title bg-red text-white text-bold text-xl">Poznámky</div>

        <div class="text-l ml-1">
            {!! nl2br(e($data->notes)) !!}
        </div>
        @endif

        <br>
        <br>
        <div class="gen-info">V Šulekove {{ date("d.m.Y", strtotime($data->created_at)) }}</div>
        <div class="clear"></div>

    </main>


</body>
</html>
