<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToPriceListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('price_lists', function (Blueprint $table) {
            $table->boolean('pdf_show_number_per_carton')->default(true)->nullable();
            $table->boolean('pdf_show_number_per_pallet')->default(true)->nullable();
            $table->text('notes')->default('')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('price_lists', function (Blueprint $table) {
            $table->dropColumn(['pdf_show_number_per_carton','pdf_show_number_per_pallet','notes']);
        });
    }
}
