<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnPriceCoefficientNailFinishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nail_finishes', function (Blueprint $table) {
            $table->dropColumn('price_coefficient');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nail_finishes', function (Blueprint $table) {
            $table->double('price_coefficient')->default(0);
        });
    }
}
