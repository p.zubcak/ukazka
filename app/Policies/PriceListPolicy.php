<?php

namespace App\Policies;

use App\Models\User;
use App\Models\PriceList;
use Illuminate\Auth\Access\HandlesAuthorization;

class PriceListPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can update the priceList.
     *
     * @param  \App\User  $user
     * @param  \App\PriceList  $priceList
     * @return mixed
     */
    public function update(User $user, PriceList $priceList)
    {
        return false;
    }

    /**
     * Determine whether the user can create the priceList.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the priceList.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        return true;
    }

}
